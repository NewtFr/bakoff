/*!
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2016 bakoff
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

// File paths.
export const CONFIG_PATH = "./config/config.json";
export const ACCOUNTS_PATH = "./config/accounts.txt";
export const ACCOUNTS_FAILED_PATH = "./config/accounts.failed.txt";

// Dofus website URLs.
export const DOFUS_URL = "http://www.dofus.com";
export const DOFUS_SECURE_URL = "https://secure.dofus.com";
export const LOGIN_POST_URL = "https://account.ankama.com/sso";
export const OGRINES_PURCHASE_URL =
    `${DOFUS_SECURE_URL}/en/buy-exchange-kama-ogrine/0-french/ogrines-purchase/create-offer`;
export const SHOP_PAYMENT_URLS = {
    en: `${DOFUS_SECURE_URL}/en/shop/payment`,
    fr: `${DOFUS_SECURE_URL}/fr/boutique/paiement`,
    es: `${DOFUS_SECURE_URL}/es/tienda/pago`,
    de: `${DOFUS_SECURE_URL}/de/shop/bezahlung`,
    it: `${DOFUS_SECURE_URL}/it/shop/pagamento`,
    pt: `${DOFUS_SECURE_URL}/pt/loja/payment`
};
export const SHOP_PAYMENT_ARTICLES = {
    en: 8896,
    fr: 8896,
    es: 8897,
    de: 8896,
    it: 8378,
    pt: 8897,
};
