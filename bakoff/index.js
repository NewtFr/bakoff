/*!
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2016 bakoff
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

import { stat } from "fs-promise";
import { writeFileSync, unlinkSync } from "fs";
import { argv } from "yargs";
import Configuration from "./configuration";
import AccountFactory from "./account/account-factory";
import Browser from "./browser";
import * as constants from "./constants";

import { getLogger } from "log4js";
const logger = getLogger("BaKOff");

/**
 * The entry class.
 */
class BaKOff {
    /**
     * The configuration.
     * @type {Configuration}
     */
    configuration = null;
    /**
     * The loaded accounts.
     * @type {Account[]}
     */
    accounts = [];

    /**
     * Initializes the tool.
     */
    async initialize() {
        logger.info("Initializing...");
        this.configuration = new Configuration(constants.CONFIG_PATH);
        logger.debug("Loading configuration...");
        await this.configuration.load();
        logger.debug("Loading accounts...");
        let accountsFile = null;
        try {
            await stat(constants.ACCOUNTS_FAILED_PATH);
            logger.info("Failed accounts list found, using this list.");
            accountsFile = constants.ACCOUNTS_FAILED_PATH;
        } catch (error) {
            accountsFile = constants.ACCOUNTS_PATH;
        }
        this.accounts = await AccountFactory.createAccountsFromFile(accountsFile);
        logger.info("Initialization complete !");
    }

    /**
     * Starts the tool.
     */
    async start() {
        logger.info("Let's do this !");

        const userAgent = this.configuration.get("user_agent");
        const ogrinesAmount = this.configuration.get("ogrines_amount");
        const kamasRate = this.configuration.get("kamas_rate");
        const server = this.configuration.get("server");
        const lang = this.configuration.get("lang", "en");

        for (const account of this.accounts) {
            logger.info("Are we there yet ?! Nooo !");
            const browser = new Browser(account, userAgent);
            try {
                await browser.login();
                if (argv.offers) {
                    await browser.createOffer(ogrinesAmount, kamasRate, server);
                } else if (argv.pages) {
                    await browser.closeCharacterPages();
                } else if (argv.subscriptions) {
                    await browser.subscribe(lang);
                } else {
                    logger.info("Nothing to do...");
                    account.operationSucceeded = true;
                }
            } catch (error) {
                // Set the error string if it is empty.
                if (!account.operationError) {
                    account.operationError = error.message;
                }
                logger.error(error);
                logger.error(`Skipping account '${account.username}'.`);
            }
        }

        logger.info("Are we there yet ?! I think we're there ! Woohoo !");
    }

    /**
     * Stops the tool. Also called on exit.
     * This method has to be synchronous, asynchronous code could break when ran at exit.
     */
    stop() {
        const failedAccountsCredentials = [];

        logger.info("=============================================");
        logger.info("================== Summary ==================");
        logger.info("=============================================");
        for (const account of this.accounts) {
            if (account.operationSucceeded) {
                logger.info(`[${account.username}] ✓`);
            } else {
                failedAccountsCredentials.push(`${account.username}:${account.password}`);
                logger.error(`[${account.username}] ✗ ${account.operationError}`);
            }
        }
        logger.info("=============================================");

        // If there are failed accounts, create a new accounts failed file from them.
        // If there are no failed accounts, delete the accounts failed file.
        if (failedAccountsCredentials.length > 0) {
            writeFileSync(constants.ACCOUNTS_FAILED_PATH, failedAccountsCredentials.join("\n"));
        } else {
            try {
                unlinkSync(constants.ACCOUNTS_FAILED_PATH);
            } catch (error) {
                // If error code is ENOENT, it's fine.
                if (error.code === "ENOENT") {
                    return;
                }
                logger.error(`Error while deleting '${constants.ACCOUNTS_FAILED_PATH}'.`, error);
            }
        }
    }
}

export default BaKOff;
