/*!
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2016 bakoff
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

import * as request from "request";
import * as cheerio from "cheerio";
import * as constants from "../constants";

import { getLogger } from "log4js";
const logger = getLogger("Browser");

/**
 * Abstraction layer around the requests made on the Dofus website.
 */
class Browser {
    /**
     * The account used to authenticate.
     * @type {Account}
     * @private
     */
    account = null;
    /**
     * The cookie jar used to store cookies for the current session.
     * @type {RequestJar}
     */
    cookieJar = null;
    /**
     * The user-agent used in the requests.
     * @type {?string}
     */
    userAgent = null;
    /**
     * The Request used to make requests, already containing default values.
     * @type {Function}
     */
    request = null;

    /**
     * Constructs a browser object.
     * @param {Account} account The account used to authenticate.
     * @param {string} userAgent The user-agent used in the requests.
     */
    constructor(account, userAgent) {
        this.account = account;
        // Create a cookie jar.
        this.cookieJar = request.jar();
        this.userAgent = userAgent;
        // Create a new Request instance containing default options included in each request.
        this.request = request.defaults({
            jar: this.cookieJar,
            followAllRedirects: true,
            headers: {
                "User-Agent": userAgent
            }
        });
    }

    /**
     * Authenticates this account on the website by logging in.
     */
    async login() {
        const form = {
            action: "login",
            from: constants.DOFUS_URL,
            login: this.account.username,
            password: this.account.password,
            remember: 0
        };

        this.log(`Connecting on '${this.account.username}'...`);

        // Load the home page before logging in to make sure we won't hit the prehome after login.
        await this.get(constants.DOFUS_URL);

        // Log in.
        let response = await this.post(constants.LOGIN_POST_URL, form);
        Browser.logResponse(response);
        let body = cheerio.load(response.body);

        // Check if we hit the TOU.
        if (response.request.uri.pathname.search(/(tou|cgu)/g) !== -1) {
            // We hit them. Validate !
            this.log("We hit the TOU. Validating...");
            response = await this.validateTOU(response, body);
            Browser.logResponse(response);
            this.log("TOU validated.");
            // Parse body again since we changed page.
            body = cheerio.load(response.body);
        }

        // Check if login failed.
        const connectLinks = body(".ak-connect-links");
        if (connectLinks.length > 0) {
            // Login failed.
            throw new Error("Login failed. Check your credentials.");
        }

            // Check for subscription
        this.checkSubscription(body);

        this.log(`Connected on '${this.account.username}' `
            + `(${this.account.getSubscriptionString()}).`);
    }

    /**
     * Creates an offer with the specified Ogrines amount and the specified kamas rate.
     * @param {number} ogrinesAmount The amount of Ogrines we want to get.
     * @param {number} kamasRate The kamas rate.
     * @param {string|null} [server=null] The server on which the offer will be created, if none
     * given, the default server will be used.
     */
    async createOffer(ogrinesAmount, kamasRate, server = null) {
        // Load the page once to get some useful data.
        let response = await this.get(constants.OGRINES_PURCHASE_URL);
        let body = cheerio.load(response.body);
        const form = {
            check_form: undefined,
            postback: undefined,
            cancel_url: undefined,
            payment: "OG",
            give: ogrinesAmount * kamasRate,
            want: ogrinesAmount,
            rate: kamasRate,
            confirm: 1,
            server: undefined
        };
        Browser.logResponse(response);

        // If we landed on the maintenance page, the account can't use the kama exchange platform.
        if (response.request.uri.pathname.search("maintenance") !== -1) {
            throw new Error("This account can't use the kama exchange platform.");
        }

        // If we landed on the server selection page, select the right server.
        if (response.request.uri.pathname.search("server-selection") !== -1) {
            this.log("Server selection needed, selecting server...");
            response = await this.selectServer(body, server);
            Browser.logResponse(response);
            this.log("Server selected.");
            // Parse body again since we changed page.
            body = cheerio.load(response.body);
        }

        // Fill some missing form attributes.
        form.check_form = body(".ak-offers-exchange input[name=check_form]").val();
        form.postback = body(".ak-offers-exchange input[name=postback]").val();
        form.cancel_url = body(".ak-offers-exchange input[name=cancel_url]").val();

        // If no server ID given, get the default server.
        const serverMatchPattern = server !== null ? `:contains(${server})` : "[selected=true]";
        const selectedServerTag = body(`select[name=dofus-server] option${serverMatchPattern}`);
        const serverId = selectedServerTag.val();
        const serverName = selectedServerTag.html();
        form.server = serverId;
        this.log(`'${serverName}' is the selected server.`);

        // We're all set, time to fire the request !
        this.log(`Creating offer for ${ogrinesAmount} Ogrines at ${kamasRate} kamas/Ogrine.`);
        const creationResponse = await this.post(constants.OGRINES_PURCHASE_URL, form);
        const creationBody = cheerio.load(creationResponse.body);

        Browser.logResponse(creationResponse);

        // Assume the offer was created. If an error occurred, the value will be overridden
        // later on.
        this.account.operationSucceeded = true;

        // Check the response.
        const error = creationBody(".ak-offers-create .error:not(.hidden)");
        if (error.length > 0) {
            // There was an error. Extract the error string.
            const errorString = error.html().trim();
            this.account.operationSucceeded = false;
            this.account.operationError = errorString;
        }

        if (this.account.operationSucceeded) {
            this.log("✓ Successfully created offer.");
        } else {
            throw new Error(`Error while creating offer : ${this.account.operationError}`);
        }
    }

    /**
     * Closes the character pages on this account.
     * This hides all the informations about the characters on their pages by modifying the pages
     * options.
     */
    async closeCharacterPages() {
        // Load the page once to get data.
        const response = await this.get(constants.DOFUS_URL);
        const body = cheerio.load(response.body);
        Browser.logResponse(response);

        // Get characters list.
        const characters = {};
        const charactersListBlock = body(".ak-characters-list .ak-character .ak-name a");
        // Fill characters list object.
        charactersListBlock.each(function appendCharacterToCharactersList() {
            const element = body(this);
            // The key is the character name, the value is the profile URL.
            characters[element.html().trim()] = element.attr("href");
        });
        // Close each character's page.
        for (const character in characters) {
            if (!characters.hasOwnProperty(character)) { continue; }
            const pageUrl = characters[character];
            this.log(`Closing page for '${character}'...`);
            const closeResponse = await this.closeCharacterPage(pageUrl);
            Browser.logResponse(closeResponse);
        }
        // If we get here, all requests succeeded.
        this.account.operationSucceeded = true;
        this.log("✓ Successfully closed character pages.");
    }

    /**
     * Subscribes the account with Ogrines, using the given article ID.
     * @param {string} [lang=en] The language to use.
     */
    async subscribe(lang = "en") {
        const shopPaymentUrl = constants.SHOP_PAYMENT_URLS[lang];
        const articleId = constants.SHOP_PAYMENT_ARTICLES[lang];
        if (typeof shopPaymentUrl === "undefined" || typeof articleId === "undefined") {
            throw new Error(`Language '${lang}' isn't supported for subscribing.`);
        }
        const form = {
            unlock: 1,
            article: articleId,
            postback: "add",
            payment: "OG"
        };
        this.log(`Subscribing with article ID '${articleId}'.`);
        const response = await this.post(shopPaymentUrl, form);
        const body = cheerio.load(response.body);
        Browser.logResponse(response);

        // Assume the account was subscribed. If an error occurred, the value will be overridden
        // later on.
        this.account.operationSucceeded = true;

        // Check the response.
        const resultBlock = body(".result .ak-tooltip-process");
        if (resultBlock.length > 0) {
            // There was an error. Extract the error string.
            const errorString = resultBlock.html().trim();
            this.account.operationSucceeded = false;
            this.account.operationError = errorString;
        }

        if (this.account.operationSucceeded) {
            this.log("✓ Successfully subscribed account.");
        } else {
            throw new Error(`Error while subscribing account : ${this.account.operationError}`);
        }
    }

    /**
     * Closes a characters's page.
     * @param {string} pageUrl The character's page URL.
     * @returns {IncomingMessage} The request response.
     * @private
     */
    async closeCharacterPage(pageUrl) {
        const manageUrl = constants.DOFUS_URL + pageUrl;
        const form = {
            hide_jobs: 1,
            history: "",
            postback: "admin"
        };
        return await this.post(manageUrl, form);
    }

    /**
     * Validates the TOU.
     * @param {IncomingMessage} previousResponse The previous response.
     * @param {Function} body The body parsed by cheerio.
     * @returns {IncomingMessage} The request response.
     * @private
     */
    async validateTOU(previousResponse, body) {
        const touVersion = body("form input[name=version_cgu]").val();
        const form = {
            postback: 1,
            from: constants.DOFUS_URL,
            version_cgu: touVersion,
            type_cgu: "CGU",
            validate_cgu: 1
        };

        return await this.post(previousResponse.request.uri.href, form);
    }

    /**
     * Selects a server from the available servers on the page. If no server ID is given, selects
     * the first server available.
     * @param {Function} body The body parsed by cheerio.
     * @param {string|null} [server=null] The server that needs to be selected.
     * @returns {IncomingMessage} The request response.
     * @private
     */
    async selectServer(body, server = null) {
        let selectionUrl;
        if (server !== null) {
            // A server name was given, search the corresponding server block.
            const serverBlock = body(`.ak-block-server:contains(${server})`);
            selectionUrl = serverBlock.attr("href");
        } else {
            // No server given, take the first one.
            const serverBlock = body(".ak-block-server").first();
            selectionUrl = serverBlock.attr("href");
        }
        // If server URL is undefined, the server wasn't found or no servers are available.
        if (typeof selectionUrl === "undefined") {
            throw new Error("Unable to select server : server not found or no servers available.");
        }

        return await this.get(constants.DOFUS_SECURE_URL + selectionUrl);
    }

    /**
     * Checks for the account's subscription and sets the appropriate values in the account.
     * @param {Function} body The body parsed by cheerio.
     * @private
     */
    checkSubscription(body) {
        const subscribe = body(".ak-game-subscribe .ak-infos-small");
        const notSubscribe = body(".ak-game-not-subscribe");
        if (notSubscribe.length > 0) {
            // We're not a subscriber.
            this.account.isSubscriber = false;
        } else if (subscribe.length > 0) {
            // We're a subscriber.
            const endDate = subscribe.html().split(" ").pop();
            this.account.isSubscriber = true;
            this.account.subscriptionEndDate = endDate;
        } else {
            // WTF are we ?
            throw new Error("Error while checking account subscription.");
        }
    }

    /**
     * Wrapper around `request.get` to add Promises.
     * @param {string} url The URL to call.
     * @returns {Promise} A promise resolved once the request is done and rejected if an error
     * occurred or the response status code isn't `200`.
     * @private
     */
    get(url) {
        return new Promise((resolve, reject) => {
            Browser.logRequest(url);
            this.request.get(url, (error, response) => {
                if (error) {
                    return reject(error);
                }
                if (response.statusCode !== 200) {
                    return reject(new Error(`Wrong status code '${response.statusCode}'.`));
                }
                return resolve(response);
            });
        });
    }

    /**
     * Wrapper around `request.post` to add Promises.
     * @param {string} url The URL to call.
     * @param {Object} form The form to post.
     * @returns {Promise} A promise resolved once the request is done and rejected if an error
     * occurred or the response status code isn't `200`.
     * @private
     */
    post(url, form) {
        return new Promise((resolve, reject) => {
            Browser.logRequest(url, form);
            this.request.post(url, { form }, (error, response) => {
                if (error) {
                    return reject(error);
                }
                if (response.statusCode !== 200) {
                    return reject(new Error(`Wrong status code '${response.statusCode}'.`));
                }
                return resolve(response);
            });
        });
    }

    /**
     * Logs a given message with a given level, prepending it with the current account.
     * @param {string} message The message.
     * @param {string} [level=info] The level.
     * @private
     */
    log(message, level = "info") {
        message = `[${this.account.username}] ${message}`;
        logger.log(level, message);
    }

    /**
     * Prints advanced logs about a request response.
     * @param {IncomingMessage} response The request response.
     * @private
     */
    static logResponse(response) {
        logger.debug(`Current URL : ${response.request.uri.href}`);
        logger.trace("Request response :", response);
    }

    /**
     * Prints advanced logs about a request we're gonna make.
     * @param {string} url The called URL.
     * @param {Object} [form=undefined] The form to POST if it is a POST request.
     * @private
     */
    static logRequest(url, form) {
        if (form) {
            logger.debug(`POST ${url}`, form);
        } else {
            logger.debug(`GET ${url}`);
        }
    }
}

export default Browser;
