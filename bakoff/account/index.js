/*!
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2016 bakoff
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

/**
 * Represents a loaded account.
 */
class Account {
    /**
     * The account username.
     * @type {?string}
     */
    username = null;
    /**
     * The account password.
     * @type {?string}
     */
    password = null;
    /**
     * Indicates whether this account has an ongoing subscription.
     * @type {boolean}
     */
    isSubscriber = false;
    /**
     * If the account has an ongoing subscription, contains its end date.
     * @type {?string}
     */
    subscriptionEndDate = null;
    /**
     * Indicates whether the current operation succeeded.
     * @type {boolean}
     */
    operationSucceeded = false;
    /**
     * If the current operation failed, contains the error.
     * @type {?string}
     */
    operationError = null;

    /**
     * Constructs an account.
     * @param {string} username The account username.
     * @param {string} password The account password.
     */
    constructor(username, password) {
        this.username = username;
        this.password = password;
    }

    /**
     * Returns a formatted string containing the informations about the account's subscription.
     * @returns {string} The formatted subscription string.
     */
    getSubscriptionString() {
        return this.isSubscriber
            ? `subscriber until ${this.subscriptionEndDate}` : "non-subscriber";
    }
}

export default Account;
