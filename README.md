BaKOff
======

A small tool that connects multiple accounts on the Dofus website to do various things in a batch manner.

Features
--------

* Logs onto accounts
* Validates the TOU if needed
* Creates offers on the kama exchange platform
* Closes character pages
* Buys one week subscriptions with Ogrines.
* Prints a summary upon exit
* Failed accounts handling

Setup
-----

* Clone the repository
* Create `config/config.json` based on `config/config.json.example`
* Create `config/accounts.txt` containing your accounts in the form `username:password`
* Execute `npm install`

Usage
-----

* `npm start -- --offers`
Will create offers on the kama exchange platform
* `npm start -- --pages`
Will close character pages.
* `npm start -- --subscriptions`
Will buy subscriptions.

There is also a bash script to simplify its usage.
`./run.sh`

Debug
-----

There are two debug modes :

* `npm run dev`, which prints debug logs for requests
* `npm run trace`, which prints extensive logs for every request and responses

You can use them in conjunction with any other mode, e.g: `npm run dev -- --pages` or `npm run trace -- --subscriptions`
